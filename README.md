** How to deploy on Firebase

Use these steps to complete it:
1. clone the repository to your local folder
2. go to [Firebase](https://firebase.google.com/) to register username and login in
3. Install Firebase CLI by running $npm install -g firebase-tools
4. Login in Google by runnig $firebase login
5. Initial your app in root directory by running @firebase init
6. Adding Firebase SDK
7. Deploying your App on Firebase cloud by runnig @firebase deploy
8. Your App is ready to visit




